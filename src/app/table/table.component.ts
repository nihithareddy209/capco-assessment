import { Component, OnInit } from '@angular/core';
import { DataService } from '../data.service';
import { DataModel } from '../data-model';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {

  items: DataModel[] = [];
  allInDisplay = false;
  columnSpan: number;
  total: number;
  loading = false;
  page = 1;
  limit = 10;
  constructor(private dataService: DataService) { }

  ngOnInit() {
    this.loading = true;
    this.loadItems();
  }
  updatePageSize(event) {
    this.allInDisplay = event.target.value === '0' ? true : false;
    this.limit = event.target.value;
    this.page = 1;
    this.loadItems();
  }

  loadItems() {
    this.dataService.loadData().subscribe(response => {
      this.loading = false;
      this.total = response.length;
      this.items = (this.getMax() === 0) ? response.slice(this.getMin() - 1) : response.slice(this.getMin() - 1, this.getMax());
      this.columnSpan = Object.keys(this.items[0] && this.items[0]).length;
    });
  }

  // minimum data index
  getMin(): number {
    return ((this.limit * this.page) - this.limit) + 1;
  }

  // maximum data index
  getMax(): number {
    let max = this.limit * this.page;
    if (max > this.total) {
      max = this.total;
    }
    return max;
  }

  // jump to specific page
  goToPage(n: number): void {
    this.page = n;
    this.loadItems();
  }

  submitRequest(rowId, rowStatus) {
    this.dataService.updateData(rowId, rowStatus);
  }
}

