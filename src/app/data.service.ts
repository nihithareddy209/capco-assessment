import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  private dataUrl = 'assets/sample_data.json';
  constructor(private http: HttpClient) {

  }
  public loadData(): Observable<any> {
    return this.http.get(this.dataUrl);
  }

  public updateData(rowId, rowStatus): Observable<any> {
    const updateDataUrl = 'mockUrl';
    return this.http.post(updateDataUrl, { id: rowId, status: rowStatus});
  }
}
